<!DOCTYPE html>
<html>
<head>
	<title>Chatworld</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css">
	

</head>
<body>
	<div class="row">
		<div class="col-1">
			<div class="logo d-flex justify-content-center">
				<span><i class="fa fa-commenting-o" aria-hidden="true"></i></span>
			</div>
			<div class="barre">
				<div class=" d-flex justify-content-center">
					<span class="bar"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
				</div>
				<div class="d-flex justify-content-center">
					<span class="bar"><a href="#"><i class="fa fa-user-o" aria-hidden="true"></i></a> </span>
				</div>
				<div class="d-flex justify-content-center">
					<span class="bar"><a href="favorite.php"><i class="fa fa-star-o" aria-hidden="true"></i></a> </span>
				</div>
				<div class="entr">
					
				</div>
				<div class="d-flex justify-content-center">
					<span class="bar"><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i> </span>
				</div>
				<div class="d-flex justify-content-center">
					<span class="bar"><a href="#"><i class="fa fa-cog" aria-hidden="true"></i></a> </span>
				</div>
				<div class="d-flex justify-content-center">
					<span class="bar"><a href="#"><i class="fa fa-power-off" aria-hidden="true"></i></a> </span>
				</div>
			</div>
		</div>
		<section class="col-3">
			<div class="discuss">
				<div class="row">
					<div class="col-6">
						<h5 class="ml-3 p-2"><b>Groups</b></h5>
					</div>
					<div class="col-6 p-2">
						
						<button type="button" class="btn btn-light ml-3 mr-2"><i class="fa fa-users" aria-hidden="true"></i></button>
						<button type="button" class="btn btn-light ml-3 mr-2"><i class="fa fa-envelope-o" aria-hidden="true"></i></button>
						
					</div>
				</div>
				<div class="row">
					<div class="col-12 d-flex justify-content-center pl-4 pr-4 pt-2">
						<input style="background-color: silver; border-radius: 5px;" type="text" class="form-control" name="" placeholder="search chat">
					</div>
				</div>

				<div class="row">
					<div class="chat active_chat">
					  	<div class="user">
							<div class="chimg col-1">
								<img class="img-circle" src="" alt="img">
							</div>
							<div class="chh col-11">
							  <h6>Henrietta Souten</h6>
							  <span style="opacity:50%;"> Lorem ipsum dolor sit amet, consectetur a... </span>
							</div>
					 	</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="chat active_chat">
					  	<div class="user">
							<div class="chimg col-1">
								<img class="img-circle" src="" alt="img">
							</div>
							<div class="chh col-11">
							  <h6>Henrietta Souten</h6>
							  <span style="opacity:50%;"> Lorem ipsum dolor sit amet, consectetur a... </span>
							</div>
					 	</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="chat active_chat">
					  	<div class="user">
							<div class="chimg col-1">
								<img class="img-circle" src="" alt="img">
							</div>
							<div class="chh col-11">
							  <h6>Henrietta Souten</h6>
							  <span style="opacity:50%;"> Lorem ipsum dolor sit amet, consectetur a... </span>
							</div>
					 	</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="chat active_chat">
					  	<div class="user">
							<div class="chimg col-1">
								<img class="img-circle" src="" alt="img">
							</div>
							<div class="chh col-11">
							  <h6>Henrietta Souten</h6>
							  <span style="opacity:50%;"> Lorem ipsum dolor sit amet, consectetur a... </span>
							</div>
					 	</div>
					</div>
				</div>
				<hr>
			</div>
		</section>

		<section class="col-8">
			<div class="row">
				<div class="col-11">
					<div class="">
						<div class="chimg">
							<img class="img-circle" src="" alt="img">
						</div>
						<div class="chh">
						  <h6><b>Karl Hubane</b></h6>
						  <span> <em>Online</em></span>
						</div>
					</div>
					<div class="d-flex justify-content-end">
							<button type="button" class="btn btn-success"><i class="fa fa-phone" aria-hidden="true"></i></button>
					</div>
						
					
				</div>
			</div>
			<hr>
			<div class="row mb-3">
				<div class="col-12 d-flex justify-content-end">
					<div class="d1">
						<span>Hey, Maher! I'm waiting for you to send me the files.</span>
						<p class="d-flex justify-content-end"><em>Am 09:34 <i class="fa fa-check" aria-hidden="true"></i><i class="fa fa-check" aria-hidden="true"></i></em></p>
					</div>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-12 d-flex justify-content-start">
					<div class="d2">
						<span>I'm sorry :( I'll send you as soon as possible</span>
						<p class="d-flex justify-content-start"><em>Pm 14:20</em></p>
					</div>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-12 d-flex justify-content-end">
					<div class="d1">
						<span>I'm waiting, thank you :).</span>
						<p class="d-flex justify-content-end"><em>Pm 14:25 <i class="fa fa-check" aria-hidden="true"></i><i class="fa fa-check" aria-hidden="true"></i></em></p>
					</div>
				</div>
			</div>
			<div class="row mb-4">
				<div class="col-12 d-flex justify-content-start">
					<div class="d2">
						<span>I'm sending files now.</span>
						<p class="d-flex justify-content-start"><em>Pm 10:20</em></p>
					</div>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-12 d-flex justify-content-start">
					<div class="d2">
						<span><i class="fa fa-file-o" aria-hidden="true"></i> important_doc.pdf </span>
						<span>Download  View</span>
						<p class="d-flex justify-content-start"><em>Pm 09:34</em></p>
					</div>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-12 d-flex justify-content-end">
					<div class="d1">
						<span>Thank you......good luck with...</span>
						<p class="d-flex justify-content-end"><em>Pm 14:50 <i class="fa fa-check" aria-hidden="true"></i><i class="fa fa-check" aria-hidden="true"></i></em></p>
					</div>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-12 d-flex justify-content-start">
					<div class="d2">
						<span>I can't wait.</span>
						<p class="d-flex justify-content-start"><em>Am 09:34</em></p>
					</div>
				</div>
			</div>
			<hr style="margin-top: 200px;">
			
			<div class="rep">
				<input style="background-color: #fff;height:50px; width:690px; border-radius: 10px;" type="" name="">
				
					<button type="button" class="btn btn-light ml-3 mr-2"><i class="fa fa-file-o" aria-hidden="true"></i></button>
					<button type="button" class="btn btn-light mr-2"><i class="fa fa-phone" aria-hidden="true"></i></button>
					<button type="submit" class="btn btn-dark mr-2"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
				
			</div>
		</section>


	</div>

	
	
	<footer>
		<div class="foot">
			<div class="row">
				<div class="col-3 hey">
					<div><a href="">A propos</a></div>
					<div><a href="">Mon abonnement</a></div>
					<div><a href="">Update</a></div>
					<div><a href="">A propos</a></div>
					<div><a href="">A propos</a></div>
					<div><a href="">A propos</a></div>
				</div>
				<div class="col-2">
					<div><a href="">A propos</a></div>
					<div><a href="">A propos</a></div>
					<div><a href="">A propos</a></div>
					<div><a href="">A propos</a></div>
					<div><a href="">A propos</a></div>
					<div><a href="">A propos</a></div>
				</div>
				<div class="col-2">
					<div><a href="">A propos</a></div>
					<div><a href="">A propos</a></div>
					<div><a href="">A propos</a></div>
					<div><a href="">A propos</a></div>
					<div><a href="">A propos</a></div>
					<div><a href="">A propos</a></div>
				</div>
				<div class="col-3">
					<div><a href="">A propos</a></div>
					<div><a href="">A propos</a></div>
					<div><a href="">A propos</a></div>
				</div>
				<div class="col-2">
					<div><a href=""><i class="fa fa-phone" aria-hidden="true"></i>  A propos</a></div>
					<div><a href=""><i class="fa fa-envelope-o" aria-hidden="true"></i> A propos</a></div>
					<div><a href=""><i class="fa fa-globe" aria-hidden="true"></i> A propos</a></div>
				</div>
			</div>
		</div>
	</footer>
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>