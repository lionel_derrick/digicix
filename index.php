<!DOCTYPE html>
<html>

<head>
    <title>CHATWORLD</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css">
</head>

<body>
    <div class="d-flex justify-content-center p-5">
        <h1 style="color:#0f056b;">Bienvenu sur chatworld</h1>
    </div>
    <div class="d-flex justify-content-center" style="background-color:#000;height:50px; color: #fff;">
        <h5 class="p-3"><i class="fa fa-sign-in" aria-hidden="true"></i> Connectez-vous</h5>
    </div>
    <div class="form d-flex justify-content-center p-3">
        <form action="log.php" method="POST">
            <div class="form-group">
                <label for="id">Email</label>
                <input type="email" class="form-control" id="" name="email" required="">
            </div>
            <div class="form-group">
                <label for="mdp">Password</label>
                <input type="password" class="form-control" id="" name="password" required="">
            </div>
            
            <button type="submit" name="submit" value="connexion" class="btn btn-primary">connexion</button>
        </form>
    </div>
    <div class="d-flex justify-content-center" style="background-color:#000;height:50px; color: #fff;">
        <h6>Vous n'avez pas de compte? Inscrivez-vous <a href="inscription.html">ici</a></h6>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>